const { merge } = require("webpack-merge");
const common = require("./webpack.common");

/** @type {import("webpack").Configuration}  */

const devConfig = {
  mode: "development",
  devServer: {
    port: 3000,
    contentBase: "../dist",
    open: "chrome"
  },
  devtool: "eval-source-map",
  module: {
    rules: [
      {
        use: [
          "style-loader",
          "css-loader"
        ],
        test: /.(sass|css|scss)$/i
      }
    ]
  }
}

module.exports = merge(common, devConfig);
