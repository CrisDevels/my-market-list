const url = "http://localhost:3001/api/list"

export const getList = () => {
  const request = fetch(url)
  return request.then(res => res.json())
}

export const postItem = (data) => {
  const request = fetch(url, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    method: "POST",
    body: JSON.stringify(data)
  })
  return request.then(res => res.json())
}