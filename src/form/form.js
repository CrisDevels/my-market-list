const MapRadio = ({type, changeProduct}) => {
  return (
    <div>
      {type.map((item, i) => {
        return (
          <label key={i} className="type-radio-label" htmlFor={item}>
            <input 
              type="radio" 
              name="type"
              id={item} 
              onChange={changeProduct}
              value={item}
              ></input>
            {item}
          </label>
        )
      })}
    </div>
  )
}

export const Form = ({type, createList, changeProduct }) => {
  return (
    <>
      <form onSubmit={createList}>

        <MapRadio type={type} changeProduct={changeProduct} />

        <input 
            name="type" 
            className="input-form"
            onChange={changeProduct}
            placeholder="Choose type item"
            ></input>

        <input 
            name="product" 
            className="input-form"
            onChange={changeProduct}
            placeholder="Add a product"
            ></input>

        <button>+</button>

        <label>or press enter</label>
      </form>
    </>
  );
}