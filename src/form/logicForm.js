import { useEffect, useState } from "react";

import { postItem } from '../services/getList';
import { Form } from './form';


export const FormIndex = ({ product, setProduct, list, setList }) => {

  const [type, setType] = useState([])

  const noRepeatt = () => {
    let types = [];
    for (let itemList of list) {
      types.push(itemList.type);
    }
    let nonRepeat = new Set(types);
    types = [...nonRepeat];
    setType(types);
  }

  useEffect(() => {
    noRepeatt();
  }, [list])

  const changeProduct = (e)=> {
    setProduct({
      ...product,
      [e.target.name] : e.target.value
    })
  }
  
  const createList = (e) => {
    e.preventDefault();
    /* postItem(product).then(data => console.log(data)) */
    setList([
      ...list,
      product
    ]);
    e.target.reset(); 
  }

  return (
    <>
      <Form type={type} createList={createList} changeProduct={changeProduct} />
    </>
  );
}