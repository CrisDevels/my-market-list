import { useEffect, useState } from "react";

export const GetTypes = () => {
  
  let DEFAULT_types = {
    yellow: "Meat",
    blue: "Grains",
    green: "Candys",
    orange: "Dog",
    purple: "Fruits"
  }
  
  const [type, setType] = useState(DEFAULT_types);
  const  [itemtype, setItemType] = useState("");

  let input = [];
  for(const color in type) {
    input.push({col: color, tp: type[color]})
  }

  const changeTypes = (e) => {
    setType({
      ...type,
      [e.target.name]: e.target.value
    })
    
  } 


  return (
    <>
      {
        input.map((element, index) => {
          return (
            <li key={index}>
              <label>
                <input type="radio" value={element.tp} name="type" />
                {element.tp}
              </label>
              <input type="text" name={element.col} onChange={changeTypes} />
            </li>
          )
        })
      }
    </>
  );
}