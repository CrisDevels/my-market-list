import { useState } from "react";
import { ListFrequently } from "./listFrecuently";

export const FrequentlyProducts = ({ list, setList }) => {

  let p = [
    {
      product: "Arroz",
      type: "Grains"
    },
    {
      product: "Lenteja",
      type: "Grains"
    },
    {
      product: "Salchicha",
      type: "Meats"
    }
  ]

  const [listFreq, setListFreq] = useState(p);
  const [productFreq, setProductFreq] = useState({
    product: "",
    type: "",
  });

  return (
    <>
      <ListFrequently 
        listFreq = {listFreq}
        list = {list} 
        setList = {setList} 
        />
    </>
  );
}