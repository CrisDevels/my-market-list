export const ItemFrecuently = ({ item, frecuenltyHandle }) => {
  return (
    <li id_pdt={item.product + item.type} className="container-frecuently">
      <span>{item.product} - {item.type}</span>
      <button id_frecuently={item.product + item.type} onClick={frecuenltyHandle} product={item.product} type_product={item.type} type="button">
        +
      </button>
    </li>
  );
}