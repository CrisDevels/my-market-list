import { ItemFrecuently } from "./itemFrecuently";

export const ListFrequently = ({ listFreq, list, setList }) => {
  
  /* const hideFrecuently = (idFrecuently) => {
    const containerF = document.querySelector(`.container-frecuently[id_pdt="${idFrecuently}"]`);
    containerF.style.display = "none";
  } */
  
  const frecuenltyHandle = (e) => {
    const event = e.target;

    const newFrecuently = {
      product: event.getAttribute("product"),
      type: event.getAttribute("type_product")
    }

    setList([
      ...list,
      newFrecuently
    ]);

    // hideFrecuently(event.getAttribute("id_frecuently"))

  }

  return (
    <ul>
      {listFreq.map((item, i) => {
        return (
          <ItemFrecuently item={item} key={i} frecuenltyHandle={frecuenltyHandle} />
        )
      })}
    </ul>
  );
}