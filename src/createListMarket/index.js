import { useState, useEffect } from 'react';
import { FormIndex } from '../form/logicForm';
import { List } from './list';

import { getList } from "../services/getList";

import '../test.css';

import { FrequentlyProducts } from '../frequentlyProducts';
import { GetTypes } from '../hooks/getTypes';

const LISTS = [
  {
    id: 1,
    name: "Work day",
    date: "2019-06-11T00:00",
    list: [
      {
        product: "Ola",
        type: "important",
        check: false
      },
      {
        product: "ke ac",
        type: "normal",
        check: false
      },
      {
        product: "bien",
        type: "regular",
        check: false
      }
    ]
  },
  {
    id: 2,
    name: "Monday market list",
    date: "2021-06-11T00:00",
    list: [
      {
        product: "Maracuya",
        type: "Fruta",
        check: false
      },
      {
        product: "Melon",
        type: "Fruta",
        check: false
      },
      {
        product: "Pollo - Pechuga",
        type: "Carnes",
        check: false
      }
    ]
  }
]

export const ListMarket = () => {

  const [list, setList] = useState([]);

  const [product, setProduct] = useState({
    product: "",
    type: "",
  });

  const [listas, setListas] = useState(LISTS);

  // How create effect to fetch DATA from API

  /* useEffect(() => {
    getList().then(data => setList(data));
  }, []) // THIS A WAY TO EJECT EFFECT ONE TIME */


  return(
    <>
      <List list={list} />
      <FormIndex list={list} setList={setList} product={product} setProduct={setProduct} />
      <ul>
        {
          listas.map((item, i)=> {
            return (
              <li key={i}>
                {item.name} - {item.date}
              </li>
            )
          })
        }
      </ul>
    </>
  );
}