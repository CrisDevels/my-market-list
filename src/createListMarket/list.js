export const List = ({ list }) => {
  return (
    <>
      <ul className="list-market-container">
        {list.map((item, i) => {
          return (
            <li className="container-product-list" key={i}>
              {item.product} - {item.type}
            </li>
          )
        })}
      </ul>
    </>
  )
}